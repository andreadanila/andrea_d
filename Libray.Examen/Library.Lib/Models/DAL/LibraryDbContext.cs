﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Lib.Models.DAL
{
    public class LibraryDbContext : DbContext
    {
        public DbSet<Librarian> Librarians { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<LBookCopy> BooksCopies { get; set; }
    }

    public LibraryDbContext ()
     {
     }


}

