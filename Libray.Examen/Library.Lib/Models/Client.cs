﻿using Common.Lib.Authtntication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Lib.Models
{
    public class Client : Users
    {
        public string Adress { get; set; }
        public string Phone { get; set; }

        public virtual List<Loan>Loans { get; set; }    
    }
}
