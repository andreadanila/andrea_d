﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Lib.Models
{
     public class Book : Entity
    {
       public string Title { get; set; }
       public string Author { get; set; }
       public string Brievieng { get; set; }
       public int FirstPublicationYear { get; set; }

        public virtual List<BookCopy>BooksCopies { get; set; }  
    }
}
