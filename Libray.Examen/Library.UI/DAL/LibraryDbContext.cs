﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Library.Lib.Models.DAL
{
    public class LibraryDbContext :DbContext
    {
        public DbSet<Librarian> Librarians { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookCopy> BooksCopies { get; set; }
    }

    public LibraryDbContext()
     {
      
     }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Librarian>().ToTable("librarians");
        builder.Entity<Book>().ToTable("books");
        builder.Entity<BookCopy>().ToTable("bookscopies");
        builder.Entity<Client>().ToTable("clients");
        builder.Entity<Loan>().ToTable("loans");

        builder.Entity<Loan>()
               .HasOne(x => x.Client)
               .WithMany(x => x.Loans)
               .HasForeignKey(x => x.ClientId);

        builder.Entity<Loan>()
            .HasOne(x => x.BookCopy)
            .WithMany(x => x.Loans)
            .HasForeignKey(x => x.BookCopyId);

        builder.Entity<BookCopy>()
            .HasOne(x => x.Book)
            .WithMany(x => x.BooksCopies)
            .HasForeignKey(x => x.BookId);

    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //optionsBuilder.UseMySQL("server=localhost;database=school;user=schooluser;password=1234");

        optionsBuilder
            .UseMySql(connectionString: @"server=localhost;database=library;uid=libraryadmin;password=1234;",
            new MySqlServerVersion(new Version(8, 0, 23)));
    }
    
}

