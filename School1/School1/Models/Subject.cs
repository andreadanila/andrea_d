﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School1.Models
{
     public class Subject: Entity
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public virtual List<Enrollment> Enrollments { get; set; }


        public Subject()
        {

        }
    }
}
