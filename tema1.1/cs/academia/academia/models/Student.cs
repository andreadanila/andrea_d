﻿namespace academia.models
{
    public class Student : Entity
    {
        public string Name { get; set; }

        public string Dni { get; set; }
    }
}
