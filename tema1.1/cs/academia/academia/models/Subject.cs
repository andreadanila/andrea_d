﻿using System;
namespace academia.models
{
    public class Subject : Entity
    {
        public string Name { get; set; }

        public string Teacher { get; set; }
    }
}
