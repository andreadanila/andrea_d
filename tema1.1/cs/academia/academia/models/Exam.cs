﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace academia.models
{
    public class Exam : Entity
    {
        public Student Student { get; set; }
        public Subject Subject { get; set; }
        public double Mark { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}
