﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var repository = new SchoolDbContext();
           

            if (repository.Students.Count() == 0)
            {
                var student1 = new Student()
                {
                    Name = "Pepe",
                    dni = "12345678a",
                    email = "p@p.com"
                };

                

                var subject1 = new Subject()
                {
                    name = "Lengua",
                    code = "S01230",
                 
                };

                repository.Students.Add(student1);
                repository.Subjects.Add(subject1);
                repository.SaveChanges();

                var registro1 = new Registration()
                {
                    studentid = student1.id,
                    subjectid = subject1.id,
                    date = DateTime.Now
                };
                repository.Registrations.Add(registro1);
                repository.SaveChanges();
            }
            DgStudents.ItemsSource = repository.Students.ToList();
            DgSubjects.ItemsSource = repository.Subjects.ToList();
            DgRegistrations.ItemsSource = repository.Registrations.ToList();
        }
    }
}
